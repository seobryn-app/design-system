import { Component, h, Host, Prop } from '@stencil/core'
import { Menu } from './types'

@Component({
  tag: 'seo-header',
  styleUrl: 'seo-header.scss',
  shadow: true,
})
export class SeoHeader {
  @Prop() menu: Menu[]
  @Prop() isLight: boolean | null = true
  @Prop({ reflect: true }) pageName: string = 'JOSE JOYA'

  gotoRoot() {
    window.location.href = '/'
  }

  render() {
    return (
      <Host is-light={this.isLight ? this.isLight : null}>
        <seo-typography variant='h5' color={this.isLight ? 'default' : 'secondary'} onClick={this.gotoRoot}>
          {this.pageName}
        </seo-typography>
        <seo-menu menu={this.menu} isLight={this.isLight ? this.isLight : null} />
      </Host>
    )
  }
}
