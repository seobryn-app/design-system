# seo-header



<!-- Auto Generated Below -->


## Properties

| Property   | Attribute   | Description | Type      | Default       |
| ---------- | ----------- | ----------- | --------- | ------------- |
| `isLight`  | `is-light`  |             | `boolean` | `true`        |
| `menu`     | --          |             | `Menu[]`  | `undefined`   |
| `pageName` | `page-name` |             | `string`  | `'JOSE JOYA'` |


## Dependencies

### Depends on

- [seo-typography](../../atoms/seo-typography)
- [seo-menu](../../molecules/seo-menu)

### Graph
```mermaid
graph TD;
  seo-header --> seo-typography
  seo-header --> seo-menu
  style seo-header fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------


