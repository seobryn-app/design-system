export interface Menu {
  id: string
  value: string
  href: string
}
