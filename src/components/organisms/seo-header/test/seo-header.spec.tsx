import { newSpecPage } from '@stencil/core/testing';
import { SeoHeader } from '../seo-header';

describe('seo-header', () => {
  it('renders', async () => {
    const page = await newSpecPage({
      components: [SeoHeader],
      html: `<seo-header></seo-header>`,
    });
    expect(page.root).toEqualHtml(`
      <seo-header>
        <mock:shadow-root>
          <slot></slot>
        </mock:shadow-root>
      </seo-header>
    `);
  });
});
