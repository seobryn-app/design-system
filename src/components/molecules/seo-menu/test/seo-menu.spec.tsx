import { newSpecPage } from '@stencil/core/testing';
import { SeoMenu } from '../seo-menu';

describe('seo-menu', () => {
  it('renders', async () => {
    const page = await newSpecPage({
      components: [SeoMenu],
      html: `<seo-menu></seo-menu>`,
    });
    expect(page.root).toEqualHtml(`
      <seo-menu>
        <mock:shadow-root>
          <slot></slot>
        </mock:shadow-root>
      </seo-menu>
    `);
  });
});
