import { Component, Event, EventEmitter, h, Host, Prop } from '@stencil/core'
import { Menu } from '../../organisms/seo-header/types'

@Component({
  tag: 'seo-menu',
  styleUrl: 'seo-menu.scss',
  shadow: true,
})
export class SeoMenu {
  @Prop() menu: Menu[]
  @Prop({ reflect: true }) isLight: boolean | null = true
  @Prop({ reflect: true }) selectedItem: string
  @Event() menuSelected: EventEmitter<Menu>

  render() {
    const handleClick = (menuItem: Menu) => {
      this.menuSelected.emit(menuItem)
    }

    return (
      <Host>
        <ul class='menu'>
          {this.menu.map((item) => (
            <li key={item.id}>
              <a onClick={() => handleClick(item)} id={item.id} class={`menu-item ${item.href === this.selectedItem ? 'selected' : ''}`.trim()}>
                {item.value}
              </a>
            </li>
          ))}
        </ul>
      </Host>
    )
  }
}
