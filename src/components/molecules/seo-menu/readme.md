# seo-menu



<!-- Auto Generated Below -->


## Properties

| Property       | Attribute       | Description | Type      | Default     |
| -------------- | --------------- | ----------- | --------- | ----------- |
| `isLight`      | `is-light`      |             | `boolean` | `true`      |
| `menu`         | --              |             | `Menu[]`  | `undefined` |
| `selectedItem` | `selected-item` |             | `string`  | `undefined` |


## Events

| Event          | Description | Type                |
| -------------- | ----------- | ------------------- |
| `menuSelected` |             | `CustomEvent<Menu>` |


## Dependencies

### Used by

 - [seo-header](../../organisms/seo-header)

### Graph
```mermaid
graph TD;
  seo-header --> seo-menu
  style seo-menu fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------


