# seo-card



<!-- Auto Generated Below -->


## Properties

| Property    | Attribute    | Description | Type                              | Default     |
| ----------- | ------------ | ----------- | --------------------------------- | ----------- |
| `align`     | `align`      |             | `"center" \| "end" \| "start"`    | `'start'`   |
| `elevation` | `elevation`  |             | `"1" \| "2" \| "3" \| "4" \| "5"` | `undefined` |
| `height`    | `height`     |             | `string`                          | `undefined` |
| `justify`   | `justify`    |             | `"center" \| "end" \| "start"`    | `'start'`   |
| `width`     | `width`      |             | `string`                          | `undefined` |
| `withHover` | `with-hover` |             | `boolean`                         | `undefined` |


## Events

| Event          | Description | Type               |
| -------------- | ----------- | ------------------ |
| `cardSelected` |             | `CustomEvent<any>` |


----------------------------------------------


