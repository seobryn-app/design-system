import { Component, Event, EventEmitter, h, Host, Listen, Prop } from '@stencil/core'
import { Align } from '../seo-typography/types'
import { Elevation } from './types'

@Component({
  tag: 'seo-card',
  styleUrl: 'seo-card.scss',
  shadow: true,
})
export class SeoCard {
  @Prop({ reflect: true }) elevation: Elevation
  @Prop({ reflect: true }) width: string
  @Prop({ reflect: true }) height: string
  @Prop() withHover: boolean | null
  @Prop({ reflect: true }) align: Align = 'start'
  @Prop({ reflect: true }) justify: Align = 'start'

  @Event() cardSelected: EventEmitter<any>

  @Listen('click')
  handleClick() {
    if (this.withHover) {
      this.cardSelected.emit()
    }
  }

  render() {
    return (
      <Host style={{ width: this.width, height: this.height }} with-hover={this.withHover}>
        <slot name='content'></slot>
        {this.withHover && <slot name='hover'></slot>}
      </Host>
    )
  }
}
