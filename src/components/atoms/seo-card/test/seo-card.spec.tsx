import { newSpecPage } from '@stencil/core/testing';
import { SeoCard } from '../seo-card';

describe('seo-card', () => {
  it('renders', async () => {
    const page = await newSpecPage({
      components: [SeoCard],
      html: `<seo-card></seo-card>`,
    });
    expect(page.root).toEqualHtml(`
      <seo-card>
        <mock:shadow-root>
          <slot></slot>
        </mock:shadow-root>
      </seo-card>
    `);
  });
});
