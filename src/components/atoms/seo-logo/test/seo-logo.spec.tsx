import { newSpecPage } from '@stencil/core/testing';
import { SeoLogo } from '../seo-logo';

describe('seo-logo', () => {
  it('renders', async () => {
    const page = await newSpecPage({
      components: [SeoLogo],
      html: `<seo-logo></seo-logo>`,
    });
    expect(page.root).toEqualHtml(`
      <seo-logo>
        <mock:shadow-root>
          <slot></slot>
        </mock:shadow-root>
      </seo-logo>
    `);
  });
});
