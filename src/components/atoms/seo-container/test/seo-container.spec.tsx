import { newSpecPage } from '@stencil/core/testing';
import { SeoContainer } from '../seo-container';

describe('seo-container', () => {
  it('renders', async () => {
    const page = await newSpecPage({
      components: [SeoContainer],
      html: `<seo-container></seo-container>`,
    });
    expect(page.root).toEqualHtml(`
      <seo-container>
        <mock:shadow-root>
          <slot></slot>
        </mock:shadow-root>
      </seo-container>
    `);
  });
});
