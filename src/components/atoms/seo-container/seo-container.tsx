import { Component, h, Host, Prop } from '@stencil/core'
import { Align } from '../seo-typography/types'

@Component({
  tag: 'seo-container',
  styleUrl: 'seo-container.scss',
  shadow: true,
})
export class SeoContainer {
  @Prop({ reflect: true }) bg: string
  @Prop({ reflect: true }) width: string
  @Prop({ reflect: true }) height: string
  @Prop({ reflect: true }) align: Align

  render() {
    return (
      <Host
        style={{
          backgroundImage: `url(${this.bg})`,
          width: this.width,
          height: this.height,
        }}
      >
        <slot />
      </Host>
    )
  }
}
