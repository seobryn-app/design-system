# seo-container



<!-- Auto Generated Below -->


## Properties

| Property | Attribute | Description | Type                           | Default     |
| -------- | --------- | ----------- | ------------------------------ | ----------- |
| `align`  | `align`   |             | `"center" \| "end" \| "start"` | `undefined` |
| `bg`     | `bg`      |             | `string`                       | `undefined` |
| `height` | `height`  |             | `string`                       | `undefined` |
| `width`  | `width`   |             | `string`                       | `undefined` |


----------------------------------------------


