import { Component, Event, EventEmitter, h, Host, Prop, State, Watch } from '@stencil/core'
import { InputType } from './types'

@Component({
  tag: 'seo-input',
  styleUrl: 'seo-input.scss',
  shadow: true,
})
export class SeoInput {
  @Prop({ reflect: true }) type: InputType = 'text'
  @Prop({ reflect: true }) placeholder = ''
  @Prop({ reflect: true }) label: string
  @Prop({ reflect: true }) min: number
  @Prop({ reflect: true }) max: number
  @Prop({ reflect: true }) width: string
  @Prop({ reflect: true }) height: string
  @Prop({ reflect: true }) value: string

  @Event() input: EventEmitter<string>

  @State() localValue: string

  @Watch('value')
  handleValue(oldVal, newVal) {
    if (oldVal !== newVal) {
      this.localValue = newVal
    }
  }

  handleChange = (evt: any) => {
    this.input.emit(evt.target.value)
    this.localValue = evt.target.value
  }

  render() {
    return (
      <Host style={{ width: this.width, height: this.height }} class={this.localValue ? 'completed' : undefined}>
        {this.label && <label>{this.label}</label>}
        <input
          type={this.type}
          placeholder={this.placeholder}
          min={this.type === 'number' ? this.min : undefined}
          max={this.type === 'number' ? this.max : undefined}
          value={this.value}
          onInput={this.handleChange}
        />
      </Host>
    )
  }
}
