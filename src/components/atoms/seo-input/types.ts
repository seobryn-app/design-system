export type InputType = 'text' | 'number' | 'email' | 'tel'
