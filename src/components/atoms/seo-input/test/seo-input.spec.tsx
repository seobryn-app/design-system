import { newSpecPage } from '@stencil/core/testing';
import { SeoInput } from '../seo-input';

describe('seo-input', () => {
  it('renders', async () => {
    const page = await newSpecPage({
      components: [SeoInput],
      html: `<seo-input></seo-input>`,
    });
    expect(page.root).toEqualHtml(`
      <seo-input>
        <mock:shadow-root>
          <slot></slot>
        </mock:shadow-root>
      </seo-input>
    `);
  });
});
