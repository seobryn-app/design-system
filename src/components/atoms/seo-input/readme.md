# seo-input



<!-- Auto Generated Below -->


## Properties

| Property      | Attribute     | Description | Type                                     | Default     |
| ------------- | ------------- | ----------- | ---------------------------------------- | ----------- |
| `height`      | `height`      |             | `string`                                 | `undefined` |
| `label`       | `label`       |             | `string`                                 | `undefined` |
| `max`         | `max`         |             | `number`                                 | `undefined` |
| `min`         | `min`         |             | `number`                                 | `undefined` |
| `placeholder` | `placeholder` |             | `string`                                 | `''`        |
| `type`        | `type`        |             | `"email" \| "number" \| "tel" \| "text"` | `'text'`    |
| `value`       | `value`       |             | `string`                                 | `undefined` |
| `width`       | `width`       |             | `string`                                 | `undefined` |


## Events

| Event   | Description | Type                  |
| ------- | ----------- | --------------------- |
| `input` |             | `CustomEvent<string>` |


----------------------------------------------


