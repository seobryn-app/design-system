import { newSpecPage } from '@stencil/core/testing';
import { SeoTypography } from '../seo-typography';

describe('seo-typography', () => {
  it('renders', async () => {
    const page = await newSpecPage({
      components: [SeoTypography],
      html: `<seo-typography></seo-typography>`,
    });
    expect(page.root).toEqualHtml(`
      <seo-typography>
        <mock:shadow-root>
          <slot></slot>
        </mock:shadow-root>
      </seo-typography>
    `);
  });
});
