# seo-typography



<!-- Auto Generated Below -->


## Properties

| Property  | Attribute | Description | Type                                                                 | Default     |
| --------- | --------- | ----------- | -------------------------------------------------------------------- | ----------- |
| `align`   | `align`   |             | `"center" \| "end" \| "start"`                                       | `'start'`   |
| `color`   | `color`   |             | `"default" \| "primary" \| "secondary"`                              | `'default'` |
| `variant` | `variant` |             | `"body1" \| "body2" \| "h1" \| "h2" \| "h3" \| "h4" \| "h5" \| "h6"` | `'body1'`   |
| `weight`  | `weight`  |             | `string`                                                             | `undefined` |


## Dependencies

### Used by

 - [seo-header](../../organisms/seo-header)

### Graph
```mermaid
graph TD;
  seo-header --> seo-typography
  style seo-typography fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------


