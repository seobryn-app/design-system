import { Component, h, Host, Prop } from '@stencil/core'
import { Color } from '../../../interfaces/common'
import { Align, Variant } from './types'

const elementMapper = {
  h1: (
    <h1>
      <slot />
    </h1>
  ),
  h2: (
    <h2>
      <slot />
    </h2>
  ),
  h3: (
    <h3>
      <slot />
    </h3>
  ),
  h4: (
    <h4>
      <slot />
    </h4>
  ),
  h5: (
    <h5>
      <slot />
    </h5>
  ),
  h6: (
    <h6>
      <slot />
    </h6>
  ),
  body1: (
    <p>
      <slot />
    </p>
  ),
  body2: (
    <span>
      <slot />
    </span>
  ),
}

@Component({
  tag: 'seo-typography',
  styleUrl: 'seo-typography.scss',
  shadow: true,
})
export class SeoTypography {
  @Prop({ reflect: true }) variant: Variant = 'body1'
  @Prop({ reflect: true }) color: Color = 'default'
  @Prop({ reflect: true }) align: Align = 'start'
  @Prop({ reflect: true }) weight: string | null

  render() {
    const El = () => elementMapper[this.variant]
    return (
      <Host>
        <El />
      </Host>
    )
  }
}
