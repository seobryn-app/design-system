import { Component, h, Host, Listen, Prop } from '@stencil/core'
import { Size } from '../../../interfaces/common'

@Component({
  tag: 'seo-button',
  styleUrl: 'seo-button.scss',
  shadow: true,
})
export class SeoButton {
  @Prop({ reflect: true }) size: Size = 'medium'
  @Prop({ reflect: true }) href?: string

  @Listen('click')
  onClickButton() {
    if (this.href) {
      window.location.href = this.href
    }
  }

  render() {
    return (
      <Host>
        <div>
          <slot></slot>
        </div>
      </Host>
    )
  }
}
