import { newSpecPage } from '@stencil/core/testing';
import { SeoButton } from '../seo-button';

describe('seo-button', () => {
  it('renders', async () => {
    const page = await newSpecPage({
      components: [SeoButton],
      html: `<seo-button></seo-button>`,
    });
    expect(page.root).toEqualHtml(`
      <seo-button>
        <mock:shadow-root>
          <slot></slot>
        </mock:shadow-root>
      </seo-button>
    `);
  });
});
