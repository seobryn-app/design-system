# seo-button

<!-- Auto Generated Below -->


## Properties

| Property | Attribute | Description | Type                             | Default     |
| -------- | --------- | ----------- | -------------------------------- | ----------- |
| `href`   | `href`    |             | `string`                         | `undefined` |
| `size`   | `size`    |             | `"large" \| "medium" \| "small"` | `'medium'`  |


----------------------------------------------


