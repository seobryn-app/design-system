# seo-divider



<!-- Auto Generated Below -->


## Properties

| Property  | Attribute | Description | Type                                    | Default     |
| --------- | --------- | ----------- | --------------------------------------- | ----------- |
| `color`   | `color`   |             | `"default" \| "primary" \| "secondary"` | `'default'` |
| `height`  | `height`  |             | `string`                                | `'1px'`     |
| `rounded` | `rounded` |             | `boolean`                               | `false`     |
| `width`   | `width`   |             | `string`                                | `undefined` |


----------------------------------------------


