import { newSpecPage } from '@stencil/core/testing';
import { SeoDivider } from '../seo-divider';

describe('seo-divider', () => {
  it('renders', async () => {
    const page = await newSpecPage({
      components: [SeoDivider],
      html: `<seo-divider></seo-divider>`,
    });
    expect(page.root).toEqualHtml(`
      <seo-divider>
        <mock:shadow-root>
          <slot></slot>
        </mock:shadow-root>
      </seo-divider>
    `);
  });
});
