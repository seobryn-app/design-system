import { Component, Element, h, Host, Prop } from '@stencil/core'
import { Color } from '../../../interfaces/common'

@Component({
  tag: 'seo-divider',
  styleUrl: 'seo-divider.scss',
  shadow: true,
})
export class SeoDivider {
  @Prop({ reflect: true }) width: string
  @Prop({ reflect: true }) height: string = '1px'
  @Prop({ reflect: true }) color: Color = 'default'
  @Prop() rounded: boolean = false

  @Element()
  host: HTMLElement

  render() {
    return (
      <Host
        rounded={this.rounded || undefined}
        style={{
          width: this.width,
          height: this.height,
        }}
      ></Host>
    )
  }
}
