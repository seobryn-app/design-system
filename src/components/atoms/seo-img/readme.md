# seo-img



<!-- Auto Generated Below -->


## Properties

| Property  | Attribute | Description | Type                             | Default     |
| --------- | --------- | ----------- | -------------------------------- | ----------- |
| `rounded` | `rounded` |             | `boolean`                        | `false`     |
| `size`    | `size`    |             | `"large" \| "medium" \| "small"` | `'medium'`  |
| `src`     | `src`     |             | `string`                         | `undefined` |


----------------------------------------------


