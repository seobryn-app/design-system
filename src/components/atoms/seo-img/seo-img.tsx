import { Component, h, Host, Prop } from '@stencil/core'
import { Size } from '../../../interfaces/common'

@Component({
  tag: 'seo-img',
  styleUrl: 'seo-img.scss',
  shadow: true,
})
export class SeoImg {
  @Prop({ reflect: true }) src: string
  @Prop({ reflect: true }) size: Size = 'medium'
  @Prop() rounded: boolean = false

  render() {
    return (
      <Host rounded={this.rounded || undefined}>
        <img src={this.src} />
      </Host>
    )
  }
}
