import { newSpecPage } from '@stencil/core/testing';
import { SeoImg } from '../seo-img';

describe('seo-img', () => {
  it('renders', async () => {
    const page = await newSpecPage({
      components: [SeoImg],
      html: `<seo-img></seo-img>`,
    });
    expect(page.root).toEqualHtml(`
      <seo-img>
        <mock:shadow-root>
          <slot></slot>
        </mock:shadow-root>
      </seo-img>
    `);
  });
});
