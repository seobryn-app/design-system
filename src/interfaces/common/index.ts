export type Color = 'primary' | 'secondary' | 'default'

export type Size = 'small' | 'medium' | 'large'
